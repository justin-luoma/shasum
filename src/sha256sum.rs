use ring::digest::{Context, SHA256};
use std::error::Error;
use std::io::Read;

pub fn sha256sum_string(s: &str) -> String {
    let mut context = Context::new(&SHA256);
    context.update(s.as_bytes());
    let digest = context.finish();

    base16ct::lower::encode_string(digest.as_ref())
}

pub fn sha256sum<R: Read>(mut reader: R) -> Result<String, Box<dyn Error>> {
    let mut context = Context::new(&SHA256);
    let mut buffer = [0; 1024];

    loop {
        let count = reader.read(&mut buffer)?;
        if count == 0 {
            break;
        }
        context.update(&buffer[..count]);
    }
    let digest = context.finish();
    let hash = base16ct::lower::encode_string(digest.as_ref());

    Ok(hash)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sha256sum_string() {
        let expected = "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08";
        assert_eq!(sha256sum_string("test"), expected);
    }

    #[test]
    fn test_sha256sum() {
        let expected = "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08";
        assert_eq!(sha256sum("test".as_bytes()).unwrap(), expected);
    }
}
