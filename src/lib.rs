use std::error::Error;
use std::io::Read;

use ring::digest::{Algorithm, Context};

pub mod sha1sum;
pub mod sha256sum;
pub mod sha384sum;
pub mod sha512sum;

fn shasum<R: Read>(mut reader: R, algorithm: &'static Algorithm) -> Result<String, Box<dyn Error>> {
    let mut context = Context::new(algorithm);
    let mut buffer = [0; 1024];

    loop {
        let count = reader.read(&mut buffer)?;
        if count == 0 {
            break;
        }
        context.update(&buffer[..count]);
    }

    let digest = context.finish();
    let hash = base16ct::lower::encode_string(digest.as_ref());

    Ok(hash)
}
