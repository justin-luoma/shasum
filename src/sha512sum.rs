use crate::shasum;
use ring::digest::SHA512;
use std::error::Error;
use std::io::Read;

pub fn sha512sum<R: Read>(reader: R) -> Result<String, Box<dyn Error>> {
    shasum(reader, &SHA512)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sha512sum() {
        let expected =
            "ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff";
        assert_eq!(sha512sum("test".as_bytes()).unwrap(), expected);
    }
}
