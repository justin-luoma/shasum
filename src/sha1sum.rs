use crate::shasum;
use ring::digest::SHA1_FOR_LEGACY_USE_ONLY;
use std::error::Error;
use std::io::Read;

pub fn sha1sum<R: Read>(reader: R) -> Result<String, Box<dyn Error>> {
    shasum(reader, &SHA1_FOR_LEGACY_USE_ONLY)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sha1sum() {
        let expected = "a94a8fe5ccb19ba61c4c0873d391e987982fbbd3";
        assert_eq!(sha1sum("test".as_bytes()).unwrap(), expected);
    }
}
