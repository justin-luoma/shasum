use std::error::Error;
use std::io::Read;

use ring::digest::SHA384;

use crate::shasum;

pub fn sha384sum<R: Read>(reader: R) -> Result<String, Box<dyn Error>> {
    shasum(reader, &SHA384)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sha384sum() {
        let expected =
            "768412320f7b0aa5812fce428dc4706b3cae50e02a64caa16a782249bfe8efc4b7ef1ccb126255d196047dfedf17a0a9";
        assert_eq!(sha384sum("test".as_bytes()).unwrap(), expected);
    }
}
