use shasum::sha256sum::sha256sum;
use std::fs::File;
use std::io::BufReader;

fn main() {
    let file = File::open("data.txt").unwrap();
    let reader = BufReader::new(file);

    let hash = sha256sum(reader).unwrap();

    println!("{}", hash);
}
